-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 30-Abr-2019 às 15:30
-- Versão do servidor: 5.7.21
-- PHP Version: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_impulsefix_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_commentmeta`
--

DROP TABLE IF EXISTS `if_commentmeta`;
CREATE TABLE IF NOT EXISTS `if_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_comments`
--

DROP TABLE IF EXISTS `if_comments`;
CREATE TABLE IF NOT EXISTS `if_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `if_comments`
--

INSERT INTO `if_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-04-29 10:36:16', '2019-04-29 13:36:16', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_links`
--

DROP TABLE IF EXISTS `if_links`;
CREATE TABLE IF NOT EXISTS `if_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_options`
--

DROP TABLE IF EXISTS `if_options`;
CREATE TABLE IF NOT EXISTS `if_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=221 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `if_options`
--

INSERT INTO `if_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://impulsefix.handgran.com.br', 'yes'),
(2, 'home', 'http://impulsefix.handgran.com.br', 'yes'),
(3, 'blogname', 'Impulsefix', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'devhcdesenvolvimentos@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:184:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"servicos/?$\";s:27:\"index.php?post_type=servico\";s:41:\"servicos/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=servico&feed=$matches[1]\";s:36:\"servicos/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=servico&feed=$matches[1]\";s:28:\"servicos/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=servico&paged=$matches[1]\";s:14:\"depoimentos/?$\";s:30:\"index.php?post_type=depoimento\";s:44:\"depoimentos/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?post_type=depoimento&feed=$matches[1]\";s:39:\"depoimentos/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?post_type=depoimento&feed=$matches[1]\";s:31:\"depoimentos/page/([0-9]{1,})/?$\";s:48:\"index.php?post_type=depoimento&paged=$matches[1]\";s:13:\"beneficios/?$\";s:29:\"index.php?post_type=beneficio\";s:43:\"beneficios/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=beneficio&feed=$matches[1]\";s:38:\"beneficios/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?post_type=beneficio&feed=$matches[1]\";s:30:\"beneficios/page/([0-9]{1,})/?$\";s:47:\"index.php?post_type=beneficio&paged=$matches[1]\";s:9:\"planos/?$\";s:25:\"index.php?post_type=plano\";s:39:\"planos/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=plano&feed=$matches[1]\";s:34:\"planos/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=plano&feed=$matches[1]\";s:26:\"planos/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=plano&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"servicos/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"servicos/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"servicos/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"servicos/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"servicos/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"servicos/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"servicos/([^/]+)/embed/?$\";s:40:\"index.php?servico=$matches[1]&embed=true\";s:29:\"servicos/([^/]+)/trackback/?$\";s:34:\"index.php?servico=$matches[1]&tb=1\";s:49:\"servicos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?servico=$matches[1]&feed=$matches[2]\";s:44:\"servicos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?servico=$matches[1]&feed=$matches[2]\";s:37:\"servicos/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?servico=$matches[1]&paged=$matches[2]\";s:44:\"servicos/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?servico=$matches[1]&cpage=$matches[2]\";s:33:\"servicos/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?servico=$matches[1]&page=$matches[2]\";s:25:\"servicos/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"servicos/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"servicos/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"servicos/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"servicos/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"servicos/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:39:\"depoimentos/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"depoimentos/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"depoimentos/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"depoimentos/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"depoimentos/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"depoimentos/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"depoimentos/([^/]+)/embed/?$\";s:43:\"index.php?depoimento=$matches[1]&embed=true\";s:32:\"depoimentos/([^/]+)/trackback/?$\";s:37:\"index.php?depoimento=$matches[1]&tb=1\";s:52:\"depoimentos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?depoimento=$matches[1]&feed=$matches[2]\";s:47:\"depoimentos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?depoimento=$matches[1]&feed=$matches[2]\";s:40:\"depoimentos/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?depoimento=$matches[1]&paged=$matches[2]\";s:47:\"depoimentos/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?depoimento=$matches[1]&cpage=$matches[2]\";s:36:\"depoimentos/([^/]+)(?:/([0-9]+))?/?$\";s:49:\"index.php?depoimento=$matches[1]&page=$matches[2]\";s:28:\"depoimentos/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:38:\"depoimentos/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:58:\"depoimentos/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"depoimentos/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"depoimentos/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:34:\"depoimentos/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:38:\"beneficios/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:48:\"beneficios/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:68:\"beneficios/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"beneficios/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:63:\"beneficios/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:44:\"beneficios/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:27:\"beneficios/([^/]+)/embed/?$\";s:42:\"index.php?beneficio=$matches[1]&embed=true\";s:31:\"beneficios/([^/]+)/trackback/?$\";s:36:\"index.php?beneficio=$matches[1]&tb=1\";s:51:\"beneficios/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?beneficio=$matches[1]&feed=$matches[2]\";s:46:\"beneficios/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?beneficio=$matches[1]&feed=$matches[2]\";s:39:\"beneficios/([^/]+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?beneficio=$matches[1]&paged=$matches[2]\";s:46:\"beneficios/([^/]+)/comment-page-([0-9]{1,})/?$\";s:49:\"index.php?beneficio=$matches[1]&cpage=$matches[2]\";s:35:\"beneficios/([^/]+)(?:/([0-9]+))?/?$\";s:48:\"index.php?beneficio=$matches[1]&page=$matches[2]\";s:27:\"beneficios/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"beneficios/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"beneficios/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"beneficios/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"beneficios/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"beneficios/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"planos/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"planos/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"planos/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"planos/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"planos/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"planos/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"planos/([^/]+)/embed/?$\";s:38:\"index.php?plano=$matches[1]&embed=true\";s:27:\"planos/([^/]+)/trackback/?$\";s:32:\"index.php?plano=$matches[1]&tb=1\";s:47:\"planos/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?plano=$matches[1]&feed=$matches[2]\";s:42:\"planos/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?plano=$matches[1]&feed=$matches[2]\";s:35:\"planos/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?plano=$matches[1]&paged=$matches[2]\";s:42:\"planos/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?plano=$matches[1]&cpage=$matches[2]\";s:31:\"planos/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?plano=$matches[1]&page=$matches[2]\";s:23:\"planos/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"planos/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"planos/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"planos/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"planos/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"planos/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:59:\"categoria-destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:54:\"categoria-destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:35:\"categoria-destaque/([^/]+)/embed/?$\";s:50:\"index.php?categoriaDestaque=$matches[1]&embed=true\";s:47:\"categoria-destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?categoriaDestaque=$matches[1]&paged=$matches[2]\";s:29:\"categoria-destaque/([^/]+)/?$\";s:39:\"index.php?categoriaDestaque=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=7&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:4:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:35:\"base-impulsefix/base-impulsefix.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:21:\"meta-box/meta-box.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'impulsefix', 'yes'),
(41, 'stylesheet', 'impulsefix', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '7', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'if_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'cron', 'a:5:{i:1556638577;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1556674577;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1556717803;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1556717804;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(113, 'theme_mods_twentysixteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1556545025;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(206, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.1.1.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.1.1.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.1\";s:7:\"version\";s:5:\"5.1.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1556633425;s:15:\"version_checked\";s:5:\"5.1.1\";s:12:\"translations\";a:0:{}}', 'no'),
(204, '_site_transient_timeout_theme_roots', '1556633588', 'no'),
(205, '_site_transient_theme_roots', 'a:2:{s:10:\"impulsefix\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(207, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1556633427;s:7:\"checked\";a:2:{s:10:\"impulsefix\";s:3:\"1.0\";s:13:\"twentysixteen\";s:3:\"1.9\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(123, '_site_transient_timeout_browser_127868b9556d0b73282ae4585eb3c66a', '1557149803', 'no'),
(124, '_site_transient_browser_127868b9556d0b73282ae4585eb3c66a', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"73.0.3683.103\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(125, '_site_transient_timeout_php_check_464f4068caea2f8f3edcc5ae59429c65', '1557149804', 'no'),
(126, '_site_transient_php_check_464f4068caea2f8f3edcc5ae59429c65', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:5:\"5.2.4\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(128, 'can_compress_scripts', '1', 'no'),
(141, 'current_theme', 'impulsefix', 'yes'),
(142, 'theme_mods_impulsefix', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(143, 'theme_switched', '', 'yes'),
(144, 'category_children', 'a:0:{}', 'yes'),
(145, '_transient_timeout_plugin_slugs', '1556719832', 'no'),
(146, '_transient_plugin_slugs', 'a:4:{i:0;s:35:\"base-impulsefix/base-impulsefix.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:21:\"meta-box/meta-box.php\";i:3;s:35:\"redux-framework/redux-framework.php\";}', 'no'),
(211, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1556644204', 'no'),
(147, 'recently_activated', 'a:0:{}', 'yes'),
(212, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4575;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:3407;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2615;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2486;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1918;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1727;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1724;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1466;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1431;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1429;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1424;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1360;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1306;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1292;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1137;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1102;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1092;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1055;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:997;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:927;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:852;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:843;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:831;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:782;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:731;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:726;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:722;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:710;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:700;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:698;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:684;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:681;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:666;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:651;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:651;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:649;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:628;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:619;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:618;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:617;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:584;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:569;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:569;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:561;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:559;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:553;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:539;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:536;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:528;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:525;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:522;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:514;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:510;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:504;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:500;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:494;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:476;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:472;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:470;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:469;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:467;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:462;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:456;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:448;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:430;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:429;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:419;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:417;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:416;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:411;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:406;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:398;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:391;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:390;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:390;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:384;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:378;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:378;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:373;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:368;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:361;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:359;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:358;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:354;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:353;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:347;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:346;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:345;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:332;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:327;}s:9:\"gutenberg\";a:3:{s:4:\"name\";s:9:\"gutenberg\";s:4:\"slug\";s:9:\"gutenberg\";s:5:\"count\";i:324;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:324;}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";i:322;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:317;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:316;}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";i:315;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:310;}s:7:\"adsense\";a:3:{s:4:\"name\";s:7:\"adsense\";s:4:\"slug\";s:7:\"adsense\";s:5:\"count\";i:309;}s:6:\"author\";a:3:{s:4:\"name\";s:6:\"author\";s:4:\"slug\";s:6:\"author\";s:5:\"count\";i:308;}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";i:308;}}', 'no'),
(214, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1556633430;s:7:\"checked\";a:4:{s:35:\"base-impulsefix/base-impulsefix.php\";s:3:\"0.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.1\";s:21:\"meta-box/meta-box.php\";s:6:\"4.17.3\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.15\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:2:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"5.0\";s:7:\"updated\";s:19:\"2018-02-03 16:12:23\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.0/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"meta-box\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"4.17.3\";s:7:\"updated\";s:19:\"2018-12-05 02:06:02\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/meta-box/4.17.3/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:3:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.17.3\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.17.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.15\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(215, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1556622631;s:7:\"version\";s:5:\"5.1.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(163, '_site_transient_timeout_browser_5d110fa8bdbbe5d74dfc9c7cac592a75', '1557188803', 'no'),
(164, '_site_transient_browser_5d110fa8bdbbe5d74dfc9c7cac592a75', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"74.0.3729.108\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(165, '_site_transient_timeout_php_check_03bb19de23a7f39f237dfd15fa323af5', '1557188804', 'no'),
(166, '_site_transient_php_check_03bb19de23a7f39f237dfd15fa323af5', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:5:\"5.2.4\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(176, 'redux_version_upgraded_from', '3.6.15', 'yes'),
(180, '_transient_timeout_select2-css_style_cdn_is_up', '1556670489', 'no'),
(181, '_transient_select2-css_style_cdn_is_up', '1', 'no'),
(182, '_transient_timeout_select2-js_script_cdn_is_up', '1556670489', 'no');
INSERT INTO `if_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(178, 'configuracao', 'a:47:{s:8:\"last_tab\";s:1:\"1\";s:8:\"opt_logo\";a:9:{s:3:\"url\";s:82:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/logoMarca.png\";s:2:\"id\";s:2:\"16\";s:6:\"height\";s:2:\"61\";s:5:\"width\";s:3:\"166\";s:9:\"thumbnail\";s:89:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/logoMarca-150x61.png\";s:5:\"title\";s:9:\"logoMarca\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:16:\"rodape_copyright\";s:55:\"Copyright 2019 ImpulseFix. Todos os direitos Reservados\";s:13:\"contato_whats\";s:11:\"41997117137\";s:15:\"destaque_titulo\";s:30:\"Suporte e reparos residenciais\";s:14:\"destaque_texto\";s:81:\"Tenha mais tempo para sua família, deixe os reparos da sua casa com a impulsefix\";s:13:\"destaque_foto\";a:9:{s:3:\"url\";s:84:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/Personagens.png\";s:2:\"id\";s:2:\"17\";s:6:\"height\";s:3:\"560\";s:5:\"width\";s:3:\"471\";s:9:\"thumbnail\";s:92:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/Personagens-150x150.png\";s:5:\"title\";s:11:\"Personagens\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:18:\"comoFunciona_video\";s:51:\"http://hudsoncarolino.com.br/video/versaoMobile.mp4\";s:19:\"comoFunciona_titulo\";s:14:\"Como funciona?\";s:18:\"comoFunciona_texto\";s:320:\"A ImpulseFix nasceu com a ideia inovadora de prestar serviços residenciais para condomínios com assinaturas mensais. Possuindo uma equipe de profissionais capacitados e legalizados, que prestam um serviço confiável e de qualidade em toda a área de Curitiba, seja em condomínios prediais, comerciais ou horizontais.\";s:17:\"comoFunciona_link\";s:1:\"#\";s:14:\"servico_titulo\";s:32:\"Qual reparo você precisa agora?\";s:17:\"servico_descricao\";s:144:\"É um fato conhecido de todos que um leitor se distrairá com o conteúdo de texto legível de uma página quando estiver página quando estiver\";s:12:\"servico_link\";s:1:\"#\";s:17:\"condominio_titulo\";s:37:\"Quer a Impulsefix no seu condomínio?\";s:16:\"condominio_texto\";s:105:\"Torne a vida dos moradores do seu condomínio mais fácil! <br> Contrato agora mesmo um de nossos planos:\";s:15:\"condominio_foto\";a:9:{s:3:\"url\";s:84:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/ferramentas.png\";s:2:\"id\";s:2:\"28\";s:6:\"height\";s:3:\"498\";s:5:\"width\";s:3:\"752\";s:9:\"thumbnail\";s:92:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/ferramentas-150x150.png\";s:5:\"title\";s:11:\"ferramentas\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"condominio_link\";s:1:\"#\";s:23:\"prestadorServico_titulo\";s:36:\"Seja um prestador de <br> serviços!\";s:22:\"prestadorServico_texto\";s:153:\"Torne a vida dos moradores do seu condomínio mais fácil! Contrato agora <br>\r\nTorne a vida dos moradores do seu condomínio mais fácil! Contrato agora\";s:21:\"prestadorServico_foto\";a:9:{s:3:\"url\";s:87:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/imgIlustrativa.png\";s:2:\"id\";s:2:\"29\";s:6:\"height\";s:3:\"418\";s:5:\"width\";s:3:\"795\";s:9:\"thumbnail\";s:95:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/imgIlustrativa-150x150.png\";s:5:\"title\";s:14:\"imgIlustrativa\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:21:\"prestadorServico_link\";s:1:\"#\";s:16:\"quemSomos_titulo\";s:11:\"Quem somos?\";s:19:\"quemSomos_descricao\";s:320:\"A ImpulseFix nasceu com a ideia inovadora de prestar serviços residenciais para condomínios com assinaturas mensais. Possuindo uma equipe de profissionais capacitados e legalizados, que prestam um serviço confiável e de qualidade em toda a área de Curitiba, seja em condomínios prediais, comerciais ou horizontais.\";s:26:\"quemSomos_foto_ilustrativa\";a:9:{s:3:\"url\";s:85:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/comoFunciona.png\";s:2:\"id\";s:2:\"37\";s:6:\"height\";s:3:\"534\";s:5:\"width\";s:3:\"532\";s:9:\"thumbnail\";s:93:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/comoFunciona-150x150.png\";s:5:\"title\";s:12:\"comoFunciona\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:11:\"viao_titulo\";s:6:\"Visão\";s:14:\"viao_descricao\";s:124:\"Ser referência como primeira empresa a criar assinaturas para condomínios, oferecendo serviços de consertos residenciais.\";s:14:\"valores_titulo\";s:7:\"Valores\";s:17:\"valores_descricao\";s:134:\"Oferecer o melhor atendimento aos nossos clientes de forma personalizada, sempre visando resolver o problema o mais rápido possível.\";s:13:\"missao_titulo\";s:7:\"Missão\";s:16:\"missao_descricao\";s:160:\"Realizar os mais variados tipos de consertos residenciais de forma rápida, e com profissionais qualificados para garantir mais tempo de lazer com sua família.\";s:20:\"comoFunciona__titulo\";s:14:\"Como funciona?\";s:22:\"comoFunciona_descricao\";s:196:\"A partir de uma assinatura coletiva entre todos os moradores do condomínio, você vai ter a seu dispor um batalhão de profissionais prontos para realizar qualquer reparo quando você necessitar.\";s:18:\"comoFunciona__link\";s:1:\"#\";s:19:\"comoFunciona__video\";s:51:\"http://hudsoncarolino.com.br/video/versaoMobile.mp4\";s:14:\"servicos__foto\";a:9:{s:3:\"url\";s:81:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/servicos.png\";s:2:\"id\";s:2:\"40\";s:6:\"height\";s:3:\"546\";s:5:\"width\";s:4:\"1440\";s:9:\"thumbnail\";s:89:\"http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/servicos-150x150.png\";s:5:\"title\";s:8:\"servicos\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:16:\"servicos__titulo\";s:9:\"Serviços\";s:18:\"servicos_descricao\";s:172:\"Nosso propósito é realizar os mais variados tipos de serviços para consertos residenciais. Pensando nisso, disponibilizamos os seguintes serviços aos nossos assinantes:\";s:23:\"trabalhe_conosco_titulo\";s:30:\"Seja um prestador de serviço!\";s:22:\"trabalhe_conosco_texto\";s:107:\"Possui conhecimento em algum dos serviços que oferecemos você pode se tornar um dos nossos profissionais!\";s:34:\"trabalhe_conosco_titulo_formulario\";s:22:\"Preencha o formulário\";s:34:\"trabalhe_conosco_titulo_beneficios\";s:36:\"Os benefícios de trabalhar conosco!\";s:12:\"plano_titulo\";s:20:\"Contrate a ImpuseFix\";s:15:\"plano_descricao\";s:38:\"Encontre o serviço que você precisa!\";s:23:\"plano_titulo_formulario\";s:19:\"Ligamos para você!\";s:26:\"plano_descricao_formulario\";s:22:\"Preencha o formulário\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(179, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:1:{s:26:\"plano_descricao_formulario\";s:38:\"Encontre o serviço que você precisa!\";}s:9:\"last_save\";i:1556636815;}', 'yes'),
(183, '_transient_select2-js_script_cdn_is_up', '1', 'no'),
(196, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(209, '_site_transient_timeout_php_check_d005457bdf39fe8b07a9eaff24a9225e', '1557236620', 'no'),
(210, '_site_transient_php_check_d005457bdf39fe8b07a9eaff24a9225e', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:5:\"5.2.4\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:1;}', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_postmeta`
--

DROP TABLE IF EXISTS `if_postmeta`;
CREATE TABLE IF NOT EXISTS `if_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=149 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `if_postmeta`
--

INSERT INTO `if_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_edit_lock', '1556546353:1'),
(4, 2, '_wp_trash_meta_status', 'publish'),
(5, 2, '_wp_trash_meta_time', '1556546507'),
(6, 2, '_wp_desired_post_slug', 'pagina-exemplo'),
(7, 7, '_edit_lock', '1556546899:1'),
(8, 7, '_wp_page_template', 'pages/inicial.php'),
(9, 9, '_edit_lock', '1556547436:1'),
(10, 9, '_wp_page_template', 'pages/planos.php'),
(11, 11, '_edit_lock', '1556547438:1'),
(12, 11, '_wp_page_template', 'default'),
(13, 13, '_edit_lock', '1556631702:1'),
(14, 13, '_wp_page_template', 'pages/sobre.php'),
(15, 15, '_edit_lock', '1556548407:1'),
(16, 16, '_wp_attached_file', '2019/04/logoMarca.png'),
(17, 16, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:166;s:6:\"height\";i:61;s:4:\"file\";s:21:\"2019/04/logoMarca.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"logoMarca-150x61.png\";s:5:\"width\";i:150;s:6:\"height\";i:61;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(18, 17, '_wp_attached_file', '2019/04/Personagens.png'),
(19, 17, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:471;s:6:\"height\";i:560;s:4:\"file\";s:23:\"2019/04/Personagens.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"Personagens-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"Personagens-252x300.png\";s:5:\"width\";i:252;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(20, 19, '_edit_last', '1'),
(21, 19, '_edit_lock', '1556586218:1'),
(22, 19, 'ImpilseFix_servico_breve_descricao', 'página quando estiver página quando'),
(23, 21, '_wp_attached_file', '2019/04/icone-servico1.png'),
(24, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:64;s:6:\"height\";i:64;s:4:\"file\";s:26:\"2019/04/icone-servico1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(25, 19, '_thumbnail_id', '21'),
(26, 19, 'ImpilseFix_servico_itens', 'a:4:{i:0;s:24:\"Instalação de tomadas;\";i:1;s:14:\"Interruptores;\";i:2;s:10:\"Chuveiros;\";i:3;s:12:\"Luminárias.\";}'),
(27, 22, '_edit_last', '1'),
(28, 22, '_edit_lock', '1556586220:1'),
(29, 23, '_wp_attached_file', '2019/04/icone-servico2.png'),
(30, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:62;s:6:\"height\";i:40;s:4:\"file\";s:26:\"2019/04/icone-servico2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(31, 24, '_wp_attached_file', '2019/04/icone-servico3.png'),
(32, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:56;s:6:\"height\";i:52;s:4:\"file\";s:26:\"2019/04/icone-servico3.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(33, 25, '_wp_attached_file', '2019/04/icone-servico4.png'),
(34, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:19;s:6:\"height\";i:53;s:4:\"file\";s:26:\"2019/04/icone-servico4.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(35, 22, '_thumbnail_id', '25'),
(36, 22, 'ImpilseFix_servico_breve_descricao', 'página quando estiver página quando'),
(37, 22, 'ImpilseFix_servico_itens', 'a:4:{i:0;s:24:\"Instalação de tomadas;\";i:1;s:14:\"Interruptores;\";i:2;s:10:\"Chuveiros;\";i:3;s:12:\"Luminárias.\";}'),
(38, 26, '_edit_last', '1'),
(39, 26, '_edit_lock', '1556586278:1'),
(40, 26, '_thumbnail_id', '24'),
(41, 26, 'ImpilseFix_servico_breve_descricao', 'página quando estiver página quando'),
(42, 26, 'ImpilseFix_servico_itens', 'a:4:{i:0;s:30:\"Instalação de suporte de TV;\";i:1;s:27:\"Instalação de prateleira;\";i:2;s:27:\"Instalação de prateleira;\";i:3;s:24:\"Instalação de móveis.\";}'),
(43, 27, '_edit_last', '1'),
(44, 27, '_edit_lock', '1556586321:1'),
(45, 27, '_thumbnail_id', '25'),
(46, 27, 'ImpilseFix_servico_breve_descricao', 'página quando estiver página quando'),
(47, 27, 'ImpilseFix_servico_itens', 'a:4:{i:0;s:24:\"Instalação de tomadas;\";i:1;s:13:\"Interruptores\";i:2;s:9:\"Chuveiros\";i:3;s:11:\"Luminárias\";}'),
(48, 28, '_wp_attached_file', '2019/04/ferramentas.png'),
(49, 28, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:752;s:6:\"height\";i:498;s:4:\"file\";s:23:\"2019/04/ferramentas.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"ferramentas-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"ferramentas-300x199.png\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(50, 29, '_wp_attached_file', '2019/04/imgIlustrativa.png'),
(51, 29, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:795;s:6:\"height\";i:418;s:4:\"file\";s:26:\"2019/04/imgIlustrativa.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"imgIlustrativa-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"imgIlustrativa-300x158.png\";s:5:\"width\";i:300;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"imgIlustrativa-768x404.png\";s:5:\"width\";i:768;s:6:\"height\";i:404;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(52, 30, '_edit_last', '1'),
(53, 30, '_edit_lock', '1556587508:1'),
(54, 31, '_wp_attached_file', '2019/04/Oval.png'),
(55, 31, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:77;s:6:\"height\";i:77;s:4:\"file\";s:16:\"2019/04/Oval.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(56, 30, '_thumbnail_id', '31'),
(57, 32, '_edit_last', '1'),
(58, 32, '_edit_lock', '1556587946:1'),
(59, 32, '_thumbnail_id', '31'),
(60, 35, '_wp_attached_file', '2019/04/pgQuemsomos.png'),
(61, 35, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1440;s:6:\"height\";i:733;s:4:\"file\";s:23:\"2019/04/pgQuemsomos.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"pgQuemsomos-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"pgQuemsomos-300x153.png\";s:5:\"width\";i:300;s:6:\"height\";i:153;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"pgQuemsomos-768x391.png\";s:5:\"width\";i:768;s:6:\"height\";i:391;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"pgQuemsomos-1024x521.png\";s:5:\"width\";i:1024;s:6:\"height\";i:521;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(62, 13, '_thumbnail_id', '35'),
(63, 36, '_menu_item_type', 'post_type'),
(64, 36, '_menu_item_menu_item_parent', '0'),
(65, 36, '_menu_item_object_id', '13'),
(66, 36, '_menu_item_object', 'page'),
(67, 36, '_menu_item_target', ''),
(68, 36, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(69, 36, '_menu_item_xfn', ''),
(70, 36, '_menu_item_url', ''),
(72, 37, '_wp_attached_file', '2019/04/comoFunciona.png'),
(73, 37, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:532;s:6:\"height\";i:534;s:4:\"file\";s:24:\"2019/04/comoFunciona.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"comoFunciona-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"comoFunciona-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(74, 38, '_menu_item_type', 'custom'),
(75, 38, '_menu_item_menu_item_parent', '0'),
(76, 38, '_menu_item_object_id', '38'),
(77, 38, '_menu_item_object', 'custom'),
(78, 38, '_menu_item_target', ''),
(79, 38, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(80, 38, '_menu_item_xfn', ''),
(81, 38, '_menu_item_url', 'http://impulsefix.handgran.com.br/servicos'),
(83, 39, '_wp_attached_file', '2019/04/fundo-plano.png'),
(84, 39, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:467;s:6:\"height\";i:214;s:4:\"file\";s:23:\"2019/04/fundo-plano.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"fundo-plano-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"fundo-plano-300x137.png\";s:5:\"width\";i:300;s:6:\"height\";i:137;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(85, 40, '_wp_attached_file', '2019/04/servicos.png'),
(86, 40, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1440;s:6:\"height\";i:546;s:4:\"file\";s:20:\"2019/04/servicos.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"servicos-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"servicos-300x114.png\";s:5:\"width\";i:300;s:6:\"height\";i:114;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"servicos-768x291.png\";s:5:\"width\";i:768;s:6:\"height\";i:291;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"servicos-1024x388.png\";s:5:\"width\";i:1024;s:6:\"height\";i:388;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(87, 41, '_edit_lock', '1556632254:1'),
(88, 43, '_menu_item_type', 'post_type'),
(89, 43, '_menu_item_menu_item_parent', '0'),
(90, 43, '_menu_item_object_id', '41'),
(91, 43, '_menu_item_object', 'page'),
(92, 43, '_menu_item_target', ''),
(93, 43, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(94, 43, '_menu_item_xfn', ''),
(95, 43, '_menu_item_url', ''),
(97, 41, '_wp_page_template', 'pages/trabalhe-conosco.php'),
(98, 44, '_edit_last', '1'),
(99, 44, '_edit_lock', '1556632298:1'),
(100, 45, '_edit_last', '1'),
(101, 45, '_edit_lock', '1556632319:1'),
(102, 46, '_edit_last', '1'),
(103, 46, '_edit_lock', '1556632810:1'),
(104, 47, '_form', '<div class=\"formulario\">[text* nomeCompleto id:nomeCompleto class:nomeCompleto placeholder \"Nome Completo\"][tel* telefone id:telefone class:telefone placeholder \"Telefone\"][email* email id:email class:email placeholder \"E-mail\"][select* Servicos id:Servicos class:Servicos \"Elétrica\" \"Hidráulica\" \"Instalação\" \"Reparos\"]<div class=\"row\"><div class=\"col-xs-5\"><p>Currículo:</p></div><div class=\"col-xs-7\"><div class=\"areaArquivo\">Anexo[file* curriculo id:curriculo class:curriculo]</div></div></div>[submit id:Enviar class:Enviar \"Enviar\"]</div>'),
(105, 47, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:27:\"Impulsefix \"[your-subject]\"\";s:6:\"sender\";s:44:\"Impulsefix <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:4:\"body\";s:190:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Impulsefix (http://impulsefix.handgran.com.br)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(106, 47, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:27:\"Impulsefix \"[your-subject]\"\";s:6:\"sender\";s:44:\"Impulsefix <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:132:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Impulsefix (http://impulsefix.handgran.com.br)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(107, 47, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(108, 47, '_additional_settings', ''),
(109, 47, '_locale', 'pt_BR'),
(112, 47, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(113, 48, '_edit_last', '1'),
(114, 48, '_edit_lock', '1556634215:1'),
(115, 49, '_wp_attached_file', '2019/04/Group-2.png'),
(116, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:27;s:6:\"height\";i:37;s:4:\"file\";s:19:\"2019/04/Group-2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(117, 50, '_wp_attached_file', '2019/04/plano2.png'),
(118, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:35;s:6:\"height\";i:38;s:4:\"file\";s:18:\"2019/04/plano2.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(119, 48, '_thumbnail_id', '49'),
(120, 48, 'ImpilseFix_plano_itens', 'a:6:{i:0;s:21:\"Mensalidade pós-paga\";i:1;s:14:\"Adesão única\";i:2;s:29:\"Acesso a todos os seguimentos\";i:3;s:35:\"Menor preço por ponto exectutável\";i:4;s:26:\"Custo deslocamento mínimo\";i:5;s:12:\"Agendamentos\";}'),
(121, 51, '_edit_last', '1'),
(122, 51, '_edit_lock', '1556637502:1'),
(123, 51, '_thumbnail_id', '50'),
(124, 51, 'ImpilseFix_plano_itens', 'a:7:{i:0;s:21:\"Mensalidade pré-paga\";i:1;s:11:\"Sem adesão\";i:2;s:29:\"Acesso a todos os seguimentos\";i:3;s:28:\"Sem custo ponto exectutável\";i:4;s:21:\"Deslocamento reduzido\";i:5;s:36:\"Orçamento diferenciado para reforma\";i:6;s:26:\"Agendamentos prioritários\";}'),
(125, 51, 'ImpilseFix_plano_maisVendido', '1'),
(126, 9, '_wp_trash_meta_status', 'publish'),
(127, 9, '_wp_trash_meta_time', '1556636505'),
(128, 9, '_wp_desired_post_slug', 'planos'),
(129, 11, '_wp_trash_meta_status', 'publish'),
(130, 11, '_wp_trash_meta_time', '1556636508'),
(131, 11, '_wp_desired_post_slug', 'planos2'),
(132, 51, 'ImpilseFix_plano_telefone', '2342342342'),
(133, 52, '_form', '<div class=\"formulario\">[text* nomeCompleto id:nomeCompleto class:nomeCompleto placeholder \"Nome Completo\"][tel* telefone id:telefone class:telefone placeholder \"Telefone\"][email* email id:email class:email placeholder \"E-mail\"][select* Servicos id:Servicos class:Servicos \"Elétrica\" \"Hidráulica\" \"Instalação\" \"Reparos\"]<div class=\"row\"><div class=\"col-xs-5\">[text* cep id:cep class:cep placeholder \"CEP\"]</div><div class=\"col-xs-7\">[text* estado id:estado class:estado placeholder \"Estado\"]</div></div>[submit id:Enviar class:Enviar \"Enviar\"]</div>'),
(134, 52, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:27:\"Impulsefix \"[your-subject]\"\";s:6:\"sender\";s:44:\"Impulsefix <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:4:\"body\";s:190:\"From: [your-name] <[your-email]>\nSubject: [your-subject]\n\nMessage Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Impulsefix (http://impulsefix.handgran.com.br)\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(135, 52, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:27:\"Impulsefix \"[your-subject]\"\";s:6:\"sender\";s:44:\"Impulsefix <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:132:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Impulsefix (http://impulsefix.handgran.com.br)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(136, 52, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(137, 52, '_additional_settings', ''),
(138, 52, '_locale', 'pt_BR'),
(139, 52, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:51:\"Invalid mailbox syntax is used in the %name% field.\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(140, 53, '_menu_item_type', 'post_type_archive'),
(141, 53, '_menu_item_menu_item_parent', '0'),
(142, 53, '_menu_item_object_id', '-36'),
(143, 53, '_menu_item_object', 'plano'),
(144, 53, '_menu_item_target', ''),
(145, 53, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(146, 53, '_menu_item_xfn', ''),
(147, 53, '_menu_item_url', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_posts`
--

DROP TABLE IF EXISTS `if_posts`;
CREATE TABLE IF NOT EXISTS `if_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `if_posts`
--

INSERT INTO `if_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-04-29 10:36:16', '2019-04-29 13:36:16', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2019-04-29 10:36:16', '2019-04-29 13:36:16', '', 0, 'http://impulsefix.handgran.com.br/?p=1', 0, 'post', '', 1),
(2, 1, '2019-04-29 10:36:16', '2019-04-29 13:36:16', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://impulsefix.handgran.com.br/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'trash', 'closed', 'open', '', 'pagina-exemplo__trashed', '', '', '2019-04-29 11:01:47', '2019-04-29 14:01:47', '', 0, 'http://impulsefix.handgran.com.br/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-04-29 10:36:16', '2019-04-29 13:36:16', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: http://impulsefix.handgran.com.br.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2019-04-29 10:36:16', '2019-04-29 13:36:16', '', 0, 'http://impulsefix.handgran.com.br/?page_id=3', 0, 'page', '', 0),
(4, 1, '2019-04-29 10:36:44', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-04-29 10:36:44', '0000-00-00 00:00:00', '', 0, 'http://impulsefix.handgran.com.br/?p=4', 0, 'post', '', 0),
(5, 1, '2019-04-29 11:01:32', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-04-29 11:01:32', '0000-00-00 00:00:00', '', 0, 'http://impulsefix.handgran.com.br/?p=5', 0, 'post', '', 0),
(6, 1, '2019-04-29 11:01:47', '2019-04-29 14:01:47', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://impulsefix.handgran.com.br/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-04-29 11:01:47', '2019-04-29 14:01:47', '', 2, 'http://impulsefix.handgran.com.br/2019/04/29/2-revision-v1/', 0, 'revision', '', 0),
(7, 1, '2019-04-29 11:02:01', '2019-04-29 14:02:01', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2019-04-29 11:08:15', '2019-04-29 14:08:15', '', 0, 'http://impulsefix.handgran.com.br/?page_id=7', 0, 'page', '', 0),
(8, 1, '2019-04-29 11:02:01', '2019-04-29 14:02:01', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-04-29 11:02:01', '2019-04-29 14:02:01', '', 7, 'http://impulsefix.handgran.com.br/2019/04/29/7-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2019-04-29 11:11:07', '2019-04-29 14:11:07', '', 'Planos', '', 'trash', 'closed', 'closed', '', 'planos__trashed', '', '', '2019-04-30 12:01:45', '2019-04-30 15:01:45', '', 0, 'http://impulsefix.handgran.com.br/?page_id=9', 0, 'page', '', 0),
(10, 1, '2019-04-29 11:11:07', '2019-04-29 14:11:07', '', 'Planos', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-04-29 11:11:07', '2019-04-29 14:11:07', '', 9, 'http://impulsefix.handgran.com.br/2019/04/29/9-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2019-04-29 11:11:24', '2019-04-29 14:11:24', '', 'Planos2', '', 'trash', 'closed', 'closed', '', 'planos2__trashed', '', '', '2019-04-30 12:01:48', '2019-04-30 15:01:48', '', 0, 'http://impulsefix.handgran.com.br/?page_id=11', 0, 'page', '', 0),
(12, 1, '2019-04-29 11:11:24', '2019-04-29 14:11:24', '', 'Planos2', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2019-04-29 11:11:24', '2019-04-29 14:11:24', '', 11, 'http://impulsefix.handgran.com.br/2019/04/29/11-revision-v1/', 0, 'revision', '', 0),
(13, 1, '2019-04-29 11:17:06', '2019-04-29 14:17:06', '<!-- wp:paragraph -->\n<p>\n\nA idéia da ImpulseFix surgiu pela necessidade que idosos, pessoas com deficiência física, e também as que não sabem como realizar consertos residenciais , tinham ao ficar dependendo de maus profissionais, e dos que não apresentam as garantias de qualidade técnica do serviço. Sem a exigida situação legal dos profissionais, ficavam sujeitos a problemas, pois nesses casos se tornariam co-responsáveis em caso de acidentes.\n\n</p>\n<!-- /wp:paragraph -->', 'Conheça a  ImpulseFix', '', 'publish', 'closed', 'closed', '', 'quem-somos', '', '', '2019-04-29 22:39:17', '2019-04-30 01:39:17', '', 0, 'http://impulsefix.handgran.com.br/?page_id=13', 0, 'page', '', 0),
(14, 1, '2019-04-29 11:17:06', '2019-04-29 14:17:06', '', 'Sobre', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2019-04-29 11:17:06', '2019-04-29 14:17:06', '', 13, 'http://impulsefix.handgran.com.br/2019/04/29/13-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2019-04-29 11:35:39', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-04-29 11:35:39', '0000-00-00 00:00:00', '', 0, 'http://impulsefix.handgran.com.br/?page_id=15', 0, 'page', '', 0),
(16, 1, '2019-04-29 21:29:42', '2019-04-30 00:29:42', '', 'logoMarca', '', 'inherit', 'open', 'closed', '', 'logomarca', '', '', '2019-04-29 21:29:42', '2019-04-30 00:29:42', '', 0, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/logoMarca.png', 0, 'attachment', 'image/png', 0),
(17, 1, '2019-04-29 21:32:43', '2019-04-30 00:32:43', '', 'Personagens', '', 'inherit', 'open', 'closed', '', 'personagens', '', '', '2019-04-29 21:32:43', '2019-04-30 00:32:43', '', 0, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/Personagens.png', 0, 'attachment', 'image/png', 0),
(18, 1, '2019-04-29 21:59:22', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-04-29 21:59:22', '0000-00-00 00:00:00', '', 0, 'http://impulsefix.handgran.com.br/?post_type=servico&p=18', 0, 'servico', '', 0),
(19, 1, '2019-04-29 22:02:37', '2019-04-30 01:02:37', 'A parte elétrica da sua casa em ordem, com os serviços de:', 'Elétrica', '', 'publish', 'closed', 'closed', '', 'eletrica', '', '', '2019-04-29 22:04:58', '2019-04-30 01:04:58', '', 0, 'http://impulsefix.handgran.com.br/?post_type=servico&#038;p=19', 0, 'servico', '', 0),
(20, 1, '2019-04-29 22:03:51', '2019-04-30 01:03:51', 'A parte elétrica da sua casa em ordem, com os serviços de:', 'Elétrica', '', 'inherit', 'closed', 'closed', '', '19-autosave-v1', '', '', '2019-04-29 22:03:51', '2019-04-30 01:03:51', '', 19, 'http://impulsefix.handgran.com.br/19-autosave-v1/', 0, 'revision', '', 0),
(21, 1, '2019-04-29 22:04:56', '2019-04-30 01:04:56', '', 'icone-servico1', '', 'inherit', 'open', 'closed', '', 'icone-servico1', '', '', '2019-04-29 22:04:56', '2019-04-30 01:04:56', '', 19, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/icone-servico1.png', 0, 'attachment', 'image/png', 0),
(22, 1, '2019-04-29 22:05:59', '2019-04-30 01:05:59', 'Sem mais vazamentos! A ImpulseFix cuida disso para você. Nós trabalhamos com o conserto de:', 'Hidráulica', '', 'publish', 'closed', 'closed', '', 'hidraulica', '', '', '2019-04-29 22:05:59', '2019-04-30 01:05:59', '', 0, 'http://impulsefix.handgran.com.br/?post_type=servico&#038;p=22', 0, 'servico', '', 0),
(23, 1, '2019-04-29 22:05:55', '2019-04-30 01:05:55', '', 'icone-servico2', '', 'inherit', 'open', 'closed', '', 'icone-servico2', '', '', '2019-04-29 22:05:55', '2019-04-30 01:05:55', '', 22, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/icone-servico2.png', 0, 'attachment', 'image/png', 0),
(24, 1, '2019-04-29 22:05:55', '2019-04-30 01:05:55', '', 'icone-servico3', '', 'inherit', 'open', 'closed', '', 'icone-servico3', '', '', '2019-04-29 22:05:55', '2019-04-30 01:05:55', '', 22, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/icone-servico3.png', 0, 'attachment', 'image/png', 0),
(25, 1, '2019-04-29 22:05:55', '2019-04-30 01:05:55', '', 'icone-servico4', '', 'inherit', 'open', 'closed', '', 'icone-servico4', '', '', '2019-04-29 22:05:55', '2019-04-30 01:05:55', '', 22, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/icone-servico4.png', 0, 'attachment', 'image/png', 0),
(26, 1, '2019-04-29 22:07:00', '2019-04-30 01:07:00', 'Não se estresse mais com a instalação de peças, deixe com a gente os serviços de:', 'Instalação', '', 'publish', 'closed', 'closed', '', 'instalacao', '', '', '2019-04-29 22:07:00', '2019-04-30 01:07:00', '', 0, 'http://impulsefix.handgran.com.br/?post_type=servico&#038;p=26', 0, 'servico', '', 0),
(27, 1, '2019-04-29 22:07:43', '2019-04-30 01:07:43', 'Tiversos tipos de reparos para sua casa, como por exemplo:', 'Reparos', '', 'publish', 'closed', 'closed', '', 'reparos', '', '', '2019-04-29 22:07:43', '2019-04-30 01:07:43', '', 0, 'http://impulsefix.handgran.com.br/?post_type=servico&#038;p=27', 0, 'servico', '', 0),
(28, 1, '2019-04-29 22:20:05', '2019-04-30 01:20:05', '', 'ferramentas', '', 'inherit', 'open', 'closed', '', 'ferramentas', '', '', '2019-04-29 22:20:05', '2019-04-30 01:20:05', '', 0, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/ferramentas.png', 0, 'attachment', 'image/png', 0),
(29, 1, '2019-04-29 22:23:18', '2019-04-30 01:23:18', '', 'imgIlustrativa', '', 'inherit', 'open', 'closed', '', 'imgilustrativa', '', '', '2019-04-29 22:23:18', '2019-04-30 01:23:18', '', 0, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/imgIlustrativa.png', 0, 'attachment', 'image/png', 0),
(30, 1, '2019-04-29 22:27:31', '2019-04-30 01:27:31', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.', 'Bruno Moares 1', '', 'publish', 'closed', 'closed', '', 'bruno-moares-1', '', '', '2019-04-29 22:27:31', '2019-04-30 01:27:31', '', 0, 'http://impulsefix.handgran.com.br/?post_type=depoimento&#038;p=30', 0, 'depoimento', '', 0),
(31, 1, '2019-04-29 22:27:28', '2019-04-30 01:27:28', '', 'Oval', '', 'inherit', 'open', 'closed', '', 'oval', '', '', '2019-04-29 22:27:28', '2019-04-30 01:27:28', '', 30, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/Oval.png', 0, 'attachment', 'image/png', 0),
(32, 1, '2019-04-29 22:27:45', '2019-04-30 01:27:45', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.', 'Bruno Moares', '', 'publish', 'closed', 'closed', '', 'bruno-moares', '', '', '2019-04-29 22:27:45', '2019-04-30 01:27:45', '', 0, 'http://impulsefix.handgran.com.br/?post_type=depoimento&#038;p=32', 0, 'depoimento', '', 0),
(33, 1, '2019-04-29 22:37:45', '2019-04-30 01:37:45', '', 'Quem Somos', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2019-04-29 22:37:45', '2019-04-30 01:37:45', '', 13, 'http://impulsefix.handgran.com.br/13-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2019-04-29 22:38:57', '2019-04-30 01:38:57', '<!-- wp:paragraph -->\n<p>\n\nA idéia da ImpulseFix surgiu pela necessidade que idosos, pessoas com deficiência física, e também as que não sabem como realizar consertos residenciais , tinham ao ficar dependendo de maus profissionais, e dos que não apresentam as garantias de qualidade técnica do serviço. Sem a exigida situação legal dos profissionais, ficavam sujeitos a problemas, pois nesses casos se tornariam co-responsáveis em caso de acidentes.\n\n</p>\n<!-- /wp:paragraph -->', 'Conheça a  ImpulseFix', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2019-04-29 22:38:57', '2019-04-30 01:38:57', '', 13, 'http://impulsefix.handgran.com.br/13-revision-v1/', 0, 'revision', '', 0),
(35, 1, '2019-04-29 22:39:13', '2019-04-30 01:39:13', '', 'pgQuemsomos', '', 'inherit', 'open', 'closed', '', 'pgquemsomos', '', '', '2019-04-29 22:39:13', '2019-04-30 01:39:13', '', 13, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/pgQuemsomos.png', 0, 'attachment', 'image/png', 0),
(36, 1, '2019-04-29 22:43:05', '2019-04-30 01:43:05', '', 'Quem Somos', '', 'publish', 'closed', 'closed', '', 'quem-somos', '', '', '2019-04-30 12:24:14', '2019-04-30 15:24:14', '', 0, 'http://impulsefix.handgran.com.br/?p=36', 1, 'nav_menu_item', '', 0),
(37, 1, '2019-04-29 22:52:37', '2019-04-30 01:52:37', '', 'comoFunciona', '', 'inherit', 'open', 'closed', '', 'comofunciona', '', '', '2019-04-29 22:52:37', '2019-04-30 01:52:37', '', 0, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/comoFunciona.png', 0, 'attachment', 'image/png', 0),
(38, 1, '2019-04-29 23:02:21', '2019-04-30 02:02:21', '', 'Serviços', '', 'publish', 'closed', 'closed', '', 'servicos', '', '', '2019-04-30 12:24:14', '2019-04-30 15:24:14', '', 0, 'http://impulsefix.handgran.com.br/?p=38', 2, 'nav_menu_item', '', 0),
(39, 1, '2019-04-30 10:44:23', '2019-04-30 13:44:23', '', 'fundo-plano', '', 'inherit', 'open', 'closed', '', 'fundo-plano', '', '', '2019-04-30 10:44:23', '2019-04-30 13:44:23', '', 0, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/fundo-plano.png', 0, 'attachment', 'image/png', 0),
(40, 1, '2019-04-30 10:44:45', '2019-04-30 13:44:45', '', 'servicos', '', 'inherit', 'open', 'closed', '', 'servicos-2', '', '', '2019-04-30 10:44:45', '2019-04-30 13:44:45', '', 0, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/servicos.png', 0, 'attachment', 'image/png', 0),
(41, 1, '2019-04-30 10:46:14', '2019-04-30 13:46:14', '', 'Trabalhe Conosco', '', 'publish', 'closed', 'closed', '', 'trabalhe-conosco', '', '', '2019-04-30 10:49:05', '2019-04-30 13:49:05', '', 0, 'http://impulsefix.handgran.com.br/?page_id=41', 0, 'page', '', 0),
(42, 1, '2019-04-30 10:46:14', '2019-04-30 13:46:14', '', 'Trabalhe Conosco', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2019-04-30 10:46:14', '2019-04-30 13:46:14', '', 41, 'http://impulsefix.handgran.com.br/41-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2019-04-30 10:46:48', '2019-04-30 13:46:48', ' ', '', '', 'publish', 'closed', 'closed', '', '43', '', '', '2019-04-30 12:24:14', '2019-04-30 15:24:14', '', 0, 'http://impulsefix.handgran.com.br/?p=43', 3, 'nav_menu_item', '', 0),
(44, 1, '2019-04-30 10:53:58', '2019-04-30 13:53:58', '<h2>Pagamento Facilitado 3</h2>\r\nVocê não precisa mais esperar o cliente te pagar diretamente! Quando você terminar o serviço, você receberá o pagamento na sua conta bancária automaticamente!', 'Renda Extra', '', 'publish', 'closed', 'closed', '', 'renda-extra', '', '', '2019-04-30 10:53:58', '2019-04-30 13:53:58', '', 0, 'http://impulsefix.handgran.com.br/?post_type=beneficio&#038;p=44', 0, 'beneficio', '', 0),
(45, 1, '2019-04-30 10:54:20', '2019-04-30 13:54:20', '<h2>Pagamento Facilitado 2</h2>\r\nVocê não precisa mais esperar o cliente te pagar diretamente! Quando você terminar o serviço, você receberá o pagamento na sua conta bancária automaticamente!', 'Horário flexível', '', 'publish', 'closed', 'closed', '', 'horario-flexivel', '', '', '2019-04-30 10:54:20', '2019-04-30 13:54:20', '', 0, 'http://impulsefix.handgran.com.br/?post_type=beneficio&#038;p=45', 0, 'beneficio', '', 0),
(46, 1, '2019-04-30 10:54:44', '2019-04-30 13:54:44', '<h2>Pagamento Facilitado 3</h2>\r\nVocê não precisa mais esperar o cliente te pagar diretamente! Quando você terminar o serviço, você receberá o pagamento na sua conta bancária automaticamente!', 'Pagamento faclitado', '', 'publish', 'closed', 'closed', '', 'pagamento-faclitado', '', '', '2019-04-30 10:54:44', '2019-04-30 13:54:44', '', 0, 'http://impulsefix.handgran.com.br/?post_type=beneficio&#038;p=46', 0, 'beneficio', '', 0),
(47, 1, '2019-04-30 11:10:31', '2019-04-30 14:10:31', '<div class=\"formulario\">[text* nomeCompleto id:nomeCompleto class:nomeCompleto placeholder \"Nome Completo\"][tel* telefone id:telefone class:telefone placeholder \"Telefone\"][email* email id:email class:email placeholder \"E-mail\"][select* Servicos id:Servicos class:Servicos \"Elétrica\" \"Hidráulica\" \"Instalação\" \"Reparos\"]<div class=\"row\"><div class=\"col-xs-5\"><p>Currículo:</p></div><div class=\"col-xs-7\"><div class=\"areaArquivo\">Anexo[file* curriculo id:curriculo class:curriculo]</div></div></div>[submit id:Enviar class:Enviar \"Enviar\"]</div>\n1\nImpulsefix \"[your-subject]\"\nImpulsefix <devhcdesenvolvimentos@gmail.com>\ndevhcdesenvolvimentos@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Impulsefix (http://impulsefix.handgran.com.br)\nReply-To: [your-email]\n\n\n\n\nImpulsefix \"[your-subject]\"\nImpulsefix <devhcdesenvolvimentos@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Impulsefix (http://impulsefix.handgran.com.br)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Trabalhe Conosco', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2019-04-30 11:15:44', '2019-04-30 14:15:44', '', 0, 'http://impulsefix.handgran.com.br/?post_type=wpcf7_contact_form&#038;p=47', 0, 'wpcf7_contact_form', '', 0),
(48, 1, '2019-04-30 11:25:55', '2019-04-30 14:25:55', '', 'Plano ImpulseFix Basic:', '', 'publish', 'closed', 'closed', '', 'plano-impulsefix-basic', '', '', '2019-04-30 11:25:55', '2019-04-30 14:25:55', '', 0, 'http://impulsefix.handgran.com.br/?post_type=plano&#038;p=48', 0, 'plano', '', 0),
(49, 1, '2019-04-30 11:25:49', '2019-04-30 14:25:49', '', 'Group 2', '', 'inherit', 'open', 'closed', '', 'group-2', '', '', '2019-04-30 11:25:49', '2019-04-30 14:25:49', '', 48, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/Group-2.png', 0, 'attachment', 'image/png', 0),
(50, 1, '2019-04-30 11:25:50', '2019-04-30 14:25:50', '', 'plano2', '', 'inherit', 'open', 'closed', '', 'plano2', '', '', '2019-04-30 11:25:50', '2019-04-30 14:25:50', '', 48, 'http://impulsefix.handgran.com.br/wp-content/uploads/2019/04/plano2.png', 0, 'attachment', 'image/png', 0),
(51, 1, '2019-04-30 11:26:46', '2019-04-30 14:26:46', '', 'Plano ImpulseFix Premium:', '', 'publish', 'closed', 'closed', '', 'plano-impulsefix-premium', '', '', '2019-04-30 12:20:07', '2019-04-30 15:20:07', '', 0, 'http://impulsefix.handgran.com.br/?post_type=plano&#038;p=51', 0, 'plano', '', 0),
(52, 1, '2019-04-30 12:22:20', '2019-04-30 15:22:20', '<div class=\"formulario\">[text* nomeCompleto id:nomeCompleto class:nomeCompleto placeholder \"Nome Completo\"][tel* telefone id:telefone class:telefone placeholder \"Telefone\"][email* email id:email class:email placeholder \"E-mail\"][select* Servicos id:Servicos class:Servicos \"Elétrica\" \"Hidráulica\" \"Instalação\" \"Reparos\"]<div class=\"row\"><div class=\"col-xs-5\">[text* cep id:cep class:cep placeholder \"CEP\"]</div><div class=\"col-xs-7\">[text* estado id:estado class:estado placeholder \"Estado\"]</div></div>[submit id:Enviar class:Enviar \"Enviar\"]</div>\n1\nImpulsefix \"[your-subject]\"\nImpulsefix <devhcdesenvolvimentos@gmail.com>\ndevhcdesenvolvimentos@gmail.com\nFrom: [your-name] <[your-email]>\r\nSubject: [your-subject]\r\n\r\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Impulsefix (http://impulsefix.handgran.com.br)\nReply-To: [your-email]\n\n\n\n\nImpulsefix \"[your-subject]\"\nImpulsefix <devhcdesenvolvimentos@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Impulsefix (http://impulsefix.handgran.com.br)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Ligamos para você', '', 'publish', 'closed', 'closed', '', 'ligamos-para-voce', '', '', '2019-04-30 12:22:20', '2019-04-30 15:22:20', '', 0, 'http://impulsefix.handgran.com.br/?post_type=wpcf7_contact_form&p=52', 0, 'wpcf7_contact_form', '', 0),
(53, 1, '2019-04-30 12:24:14', '2019-04-30 15:24:14', '', 'Planos', '', 'publish', 'closed', 'closed', '', 'planos', '', '', '2019-04-30 12:24:14', '2019-04-30 15:24:14', '', 0, 'http://impulsefix.handgran.com.br/?p=53', 4, 'nav_menu_item', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_termmeta`
--

DROP TABLE IF EXISTS `if_termmeta`;
CREATE TABLE IF NOT EXISTS `if_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_terms`
--

DROP TABLE IF EXISTS `if_terms`;
CREATE TABLE IF NOT EXISTS `if_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `if_terms`
--

INSERT INTO `if_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Menu Principal', 'menu-principal', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_term_relationships`
--

DROP TABLE IF EXISTS `if_term_relationships`;
CREATE TABLE IF NOT EXISTS `if_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `if_term_relationships`
--

INSERT INTO `if_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(36, 2, 0),
(38, 2, 0),
(43, 2, 0),
(53, 2, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_term_taxonomy`
--

DROP TABLE IF EXISTS `if_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `if_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `if_term_taxonomy`
--

INSERT INTO `if_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_usermeta`
--

DROP TABLE IF EXISTS `if_usermeta`;
CREATE TABLE IF NOT EXISTS `if_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `if_usermeta`
--

INSERT INTO `if_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'impulsefix'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'if_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'if_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy'),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:3:{s:64:\"fa5bba45dada6d98b76599333c8afd90ce059aea0f4d496d41cee26301cf4ea8\";a:4:{s:10:\"expiration\";i:1556717802;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36\";s:5:\"login\";i:1556545002;}s:64:\"feab51950ef82cfe3874ecc86beb7c2972ab5c17d7e7077ae1c917ea3fc675a3\";a:4:{s:10:\"expiration\";i:1556756802;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36\";s:5:\"login\";i:1556584002;}s:64:\"56fa641e3d902c3dfee4f5e73b68b8e40a02df8912945f911b90a53764c8224b\";a:4:{s:10:\"expiration\";i:1556804619;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36\";s:5:\"login\";i:1556631819;}}'),
(17, 1, 'if_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'closedpostboxes_page', 'a:0:{}'),
(19, 1, 'metaboxhidden_page', 'a:5:{i:0;s:10:\"postcustom\";i:1;s:16:\"commentstatusdiv\";i:2;s:11:\"commentsdiv\";i:3;s:7:\"slugdiv\";i:4;s:9:\"authordiv\";}'),
(20, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(21, 1, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(22, 1, 'if_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1556670490;}'),
(23, 1, 'if_user-settings', 'libraryContent=browse'),
(24, 1, 'if_user-settings-time', '1556584205'),
(25, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(26, 1, 'metaboxhidden_nav-menus', 'a:4:{i:0;s:21:\"add-post-type-servico\";i:1;s:24:\"add-post-type-depoimento\";i:2;s:12:\"add-post_tag\";i:3;s:21:\"add-categoriaDestaque\";}'),
(27, 1, 'nav_menu_recently_edited', '2');

-- --------------------------------------------------------

--
-- Estrutura da tabela `if_users`
--

DROP TABLE IF EXISTS `if_users`;
CREATE TABLE IF NOT EXISTS `if_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `if_users`
--

INSERT INTO `if_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'impulsefix', '$P$B6mBD6JHhDrafSbBWQ8GG7odRecXbN0', 'impulsefix', 'devhcdesenvolvimentos@gmail.com', '', '2019-04-29 13:36:16', '', 0, 'impulsefix');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
