<?php

/**
 * Plugin Name: Base ImpulseFix
 * Description: Controle base do tema ImpulseFix.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseImpilseFix () {

		// TIPOS DE CONTEÚDO
		conteudosImpilseFix();

		taxonomiaImpilseFix();

		metaboxesImpilseFix();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosImpilseFix (){

		// TIPOS DE CONTEÚDO
		tipoServicos();

		tipoDepoimento();

		tipoBeneficios();

		tipoPlanos();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoServicos() {

		$rotuloServicos = array(
								'name'               => 'Serviços',
								'singular_name'      => 'serviço',
								'menu_name'          => 'Serviços',
								'name_admin_bar'     => 'Serviços',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo serviço',
								'new_item'           => 'Novo serviço',
								'edit_item'          => 'Editar serviço',
								'view_item'          => 'Ver serviço',
								'all_items'          => 'Todos os serviços',
								'search_items'       => 'Buscar serviço',
								'parent_item_colon'  => 'Dos serviços',
								'not_found'          => 'Nenhum serviço cadastrado.',
								'not_found_in_trash' => 'Nenhum serviço na lixeira.'
							);

		$argServicos 	= array(
								'labels'             => $rotuloServicos,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'servicos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('servico', $argServicos);

	}

	// CUSTOM POST TYPE DEPOIMENTO
	function tipoDepoimento() {

		$rotulosDepoimento = array(

								'name'               => 'Depoimentos',
								'singular_name'      => 'Depoimento',
								'menu_name'          => 'Depoimentos',
								'name_admin_bar'     => 'Depoimentos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo depoimento',
								'new_item'           => 'Novo depoimento',
								'edit_item'          => 'Editar depoimento',
								'view_item'          => 'Ver depoimento',
								'all_items'          => 'Todos os depoimentos',
								'search_items'       => 'Buscar depoimentos',
								'parent_item_colon'  => 'Dos depoimentos',
								'not_found'          => 'Nenhum depoimento cadastrado.',
								'not_found_in_trash' => 'Nenhum depoimento na lixeira.'
							);

		$argsDepoimento 	= array(
								'labels'             => $rotulosDepoimento,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-heart',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'depoimentos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title', 'thumbnail', 'editor' )
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('depoimento', $argsDepoimento);

	}

	// CUSTOM POST TYPE BENEFÍCIOS
	function tipoBeneficios() {

		$rotulosBeneficios = array(

								'name'               => 'Benefícios',
								'singular_name'      => 'benefício',
								'menu_name'          => 'Benefícios',
								'name_admin_bar'     => 'Benefícios',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo benefício',
								'new_item'           => 'Novo benefício',
								'edit_item'          => 'Editar benefício',
								'view_item'          => 'Ver benefício',
								'all_items'          => 'Todos os benefícios',
								'search_items'       => 'Buscar benefícios',
								'parent_item_colon'  => 'Dos benefícios',
								'not_found'          => 'Nenhum benefício cadastrado.',
								'not_found_in_trash' => 'Nenhum benefício na lixeira.'
							);

		$argsBeneficios 	= array(
								'labels'             => $rotulosBeneficios,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-heart',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'beneficios' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor' )
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('beneficio', $argsBeneficios);

	}

	// CUSTOM POST TYPE PLANOS
	function tipoPlanos() {

		$rotulosPlanos = array(

								'name'               => 'Planos',
								'singular_name'      => 'plano',
								'menu_name'          => 'Planos',
								'name_admin_bar'     => 'Planos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo plano',
								'new_item'           => 'Novo plano',
								'edit_item'          => 'Editar plano',
								'view_item'          => 'Ver plano',
								'all_items'          => 'Todos os planos',
								'search_items'       => 'Buscar planos',
								'parent_item_colon'  => 'Dos planos',
								'not_found'          => 'Nenhum plano cadastrado.',
								'not_found_in_trash' => 'Nenhum plano na lixeira.'
							);

		$argsPlanos 	= array(
								'labels'             => $rotulosPlanos,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-heart',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'planos' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('plano', $argsPlanos);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaImpilseFix () {		
		taxonomiaCategoriaDestaque();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesImpilseFix(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'ImpilseFix_';

			// METABOX DE SERVIÇOS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxServico',
				'title'			=> 'Detalhes do serviço',
				'pages' 		=> array( 'servico' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Breve descrição: ',
						'id'    => "{$prefix}servico_breve_descricao",
						'desc'  => 'Faça uma breve descrição para página inicial',
						'type'  => 'text',
					),	
					array(
						'name'  => 'Itens do serviço: ',
						'id'    => "{$prefix}servico_itens",
						'desc'  => 'Montar a lista de itens do serviço',
						'type'  => 'text',
						'clone'  => true,
					),	
					
				),
			);

			// METABOX DE PLANOS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxPlano',
				'title'			=> 'Detalhes do planos',
				'pages' 		=> array( 'plano' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Itens do plano: ',
						'id'    => "{$prefix}plano_itens",
						'desc'  => 'Montar a lista de itens do plano',
						'type'  => 'text',
						'clone'  => true,
					),	

					array(
						'name'  => 'Mais vendido: ',
						'id'    => "{$prefix}plano_maisVendido",
						'desc'  => 'Marque se for o mais vendido',
						'type'  => 'checkbox',
					),
					array(
						'name'  => 'Telefone personalizado: ',
						'id'    => "{$prefix}plano_telefone",
						'desc'  => 'Caso deixe esse campo vazio, é configurado o telefone pradrão do site',
						'type'  => 'text',
					),	
					
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesImpilseFix(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerImpilseFix(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseImpilseFix');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseImpilseFix();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );