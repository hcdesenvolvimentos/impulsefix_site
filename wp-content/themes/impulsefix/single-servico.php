<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package impulsefix
 */

get_header();
?>

	<!-- PÁGINA QUEM SOMOS  -->

<div class="pg pg-servico">

	<section class="bannerDestaqueInternas ">

		<h4 class="hidden"><?php echo get_the_title(); ?></h4>

		<div class="containerLarge ">

			<article>

				<h1 class="tituloPagina text-left"><?php echo get_the_title(); ?></h1>



				<?php 	while ( have_posts() ) :

				the_post(); ?>

					<div class="textoPagina text-left"><?php echo get_the_content(); ?></div>

				<?php endwhile; ?>

			</article>

			<figure>

				<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title(); ?>">

				 <figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>

			</figure>

		</div>

	</section>

	<div class="containerLarge">
		<section class="prestadorServico">
			<h1 class="subTitulo text-center">Ligamos para você!</h1>
			<p class="textoPagina text-center" style="max-width: 100%;">Preencha o formulário</p>
			<div class="formulario">
				<input type="text" placeholder="Nome Completo">
				<input type="tel" name="" placeholder="Telefone">
				<input type="email" placeholder="E-mail">
				<select>
					<option>Qual serviço você prefere?</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
				</select>
				<div class="row">
					<div class="col-xs-6">
					  <input type="text" placeholder="CEP">
					</div>			
					<div class="col-xs-6">
						
						<input type="text" placeholder="Estado">
					</div>
				</div>
				<input type="submit" value="Pronto" name="">
			</div>
		</section>
	</div>

</div>

<?php

get_footer();
