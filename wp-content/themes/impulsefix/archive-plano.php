<?php

/**

 * The template for displaying archive pages

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package impulsefix

 */



get_header();

?>

<!-- PÁGINA DE PLANOS -->

		<div class="pg pg-contrate1 planos">

			<section class="secao-planos">

				<h6 class="hidden"><?php echo $configuracao['plano_titulo'] ?></h6>

				<div class="containerLarge">

					<h1 class="tituloPagina"><?php echo $configuracao['plano_titulo'] ?></h1>

					<span class="textoPagina"><?php echo $configuracao['plano_descricao'] ?></span>

					<ul>

						<?php 

							//LOOP DE POST SERVIÇOS

							$planos = new WP_Query( array( 'post_type' => 'plano', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );

							while ( $planos->have_posts() ) : $planos->the_post();

						 ?>

						<li>

							<?php if(rwmb_meta('ImpilseFix_plano_maisVendido') == "1"): ?>

							<span class="mais-vendido"><img src="<?php echo get_template_directory_uri(); ?>/img/star.svg" alt="<?php echo get_the_title() ?>">Mais vendido</span>

							<?php endif; ?>

							<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>" class="icone">

							<h5><?php echo get_the_title() ?></h5>

							<ul class="ul-info-plano">

								<?php  

									$plano_itens = rwmb_meta('ImpilseFix_plano_itens'); 

									foreach ($plano_itens as $plano_itens):

										$plano_itens = $plano_itens;

								?>

								<li class="li-info-plano"><?php echo $plano_itens ?></li>

								<?php endforeach; ?>

							</ul>

							

							<?php 

							



							 	if (false) {

							 		$telefone = rwmb_meta('ImpilseFix_plano_telefone');

							 	}else{

							 		$telefone = "https://api.whatsapp.com/send?l=pt&amp;phone=".$configuracao['contato_whats'];

							 	}

							?>

							<div class="btnLink backgroundVerde">

								<a href="<?php echo $telefone ?>">
									<p class="textoBtnPlanos">Quero esse plano</p>
									<span class="phone"><?php echo $telefone ?></span>
								</a>

							</div>

						</li>

						<?php endwhile; wp_reset_query(); ?>

					</ul>

				</div>

			</section>

			

			<div class="containerLarge">
				<section class="prestadorServico">	

					<h1 class="subTitulo"><?php echo $configuracao['plano_titulo_formulario'] ?>!</h1>

					<p class="textoPagina"><?php echo $configuracao['plano_descricao_formulario'] ?></p>



					<div class="formulario">

						<?php echo do_shortcode('[contact-form-7 id="52" title="Ligamos para você"]'); ?>

					</div>

				</section>

			</div>



		</div>



<?php

get_footer();

