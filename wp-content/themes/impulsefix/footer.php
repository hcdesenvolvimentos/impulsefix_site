<?php

/**

 * The template for displaying the footer

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package impulsefix

 */



global $configuracao;



?>

		<!-- FOOTER -->

		<footer class="rodape">

			

			<figure>

				<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php bloginfo(); ?>">

			 	<figcaption class="hidden"><?php bloginfo(); ?></figcaption>

			</figure>

			

			<nav>

				<?php wp_nav_menu(); ?>

			</nav>



			<div class="btnWhts backgroundVerde">

				<a href="https://api.whatsapp.com/send?l=pt&phone=55<?php echo $configuracao['contato_whats'] ?>" target="_blank">Agendamento pelo Whats</a>

			</div>



			<div class="contatoNumero">

				<span>Telefone: (41) 4106-2114</span> 

				<span>E-mail: comercial@impulsefix.com.br</span>

			</div>



			<p class="copyright"><?php echo $configuracao['rodape_copyright'] ?></p>


			<figure>
				<img src="<?php echo get_template_directory_uri(); ?>/img/selo.jpeg" style="    width: 125px;">
			</figure>
		</footer>

		<?php wp_footer(); ?>

	<div class="modalFormularioSucess" id="modalFormularioSucess">
		<figure>
			<img src="<?php echo get_template_directory_uri(); ?>/img/Pop-up-done.png">
		</figure>
	</div>

	<div class="modalFormularioError" id="modalFormularioError">
		<figure>
			<img src="<?php echo get_template_directory_uri(); ?>/img/Pop-up-error.png">
		</figure>
	</div>
	</body>

</html>