<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package impulsefix
 */

get_header();
?>
<div class="pg pg-servicos">
			
	<section class="bannerDestaqueInternas" style="">
		<h4 class="hidden"><?php echo $configuracao['servicos__titulo'] ?></h4>

		<div class="containerLarge">
			<article>
				<h6 class="tituloPagina"><?php echo $configuracao['servicos__titulo'] ?></h6>
				<p class="textoPagina"><?php echo $configuracao['servicos_descricao'] ?></p>
			</article>

			<figure>
				<img src="<?php echo $configuracao['servicos__foto']['url'] ?>" alt="<?php echo $configuracao['servicos__titulo'] ?>">
				 <figcaption class="hidden"><?php echo $configuracao['servicos__titulo'] ?></figcaption>
			</figure>
		</div>
	
	</section>

	<section class="secao-servicos">
		<h6 class="hidden">SEÇÃO DE SERVIÇOS</h6>
		<ul>
			<?php 
				//LOOP DE POST SERVIÇOS
				$servicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
				while ( $servicos->have_posts() ) : $servicos->the_post();
			 ?>
			<li class="hvr-float-shadow">
				<a href="<?php echo get_permalink() ?>">
					<figure>
						<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
						 <figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
					</figure>
					<h2><?php echo get_the_title() ?></h2>
					<span class="textoPagina"><?php customExcerpt(130); ?></span>
					<div class="item">
						<?php  
							$servico_itens = rwmb_meta('ImpilseFix_servico_itens'); 
							foreach ($servico_itens as $servico_itens):
								$servico_itens = $servico_itens;
						?>
						<p><?php echo $servico_itens ?></p>
						<?php endforeach; ?>
					</div>
				</a>
			</li>
			<?php endwhile; wp_reset_query(); ?>
		</ul>
	</section>

</div>
<?php
get_footer();
