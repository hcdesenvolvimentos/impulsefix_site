<?php

/**

 * The header for our theme

 *

 * This is the template that displays all of the <head> section and everything up until <div id="content">

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package impulsefix

 */



global $configuracao;



?>

<!DOCTYPE html>

<html lang="pt-br">

<head>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" /> 

<!-- begin olark code -->
<script type="text/javascript" async> ;(function(o,l,a,r,k,y){if(o.olark)return; r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0]; y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r); y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)}; y.extend=function(i,j){y("extend",i,j)}; y.identify=function(i){y("identify",k.i=i)}; y.configure=function(i,j){y("configure",i,j);k.c[i]=j}; k=y._={s:[],t:[+new Date],c:{},l:a}; })(window,document,"static.olark.com/jsclient/loader.js");

/* custom configuration goes here (www.olark.com/documentation) */
olark.identify('7805-869-10-5440');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K6V9BJZ');</script>
<!-- End Google Tag Manager -->

<!-- end olark code -->
<?php wp_head(); ?>

</head>



<body <?php body_class(); ?>>
	
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K6V9BJZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="modalCntatoSite">
	<button id="fechar"><img src="<?php echo get_template_directory_uri(); ?>/img/cancel.svg"></button>
	<section class="prestadorServico">	

		<h1 class="tituloPagina text-center">Contrate Impulsefix</h1>

		<?php echo do_shortcode('[contact-form-7 id="64" title="Formulário Botões de contato"]'); ?>

	</section>
</div>

<!-- TOPO -->

<header class="topo">

	<div class="containerLarge">

		

		<button id="openMenu">

			<span></span>

			<span></span>

			<span></span>

		</button>



		<!-- LOGO -->

		<figure>

			<a href="<?php echo home_url('/'); ?>">

				<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php bloginfo(); ?>">

			</a>

		 	<figcaption class="hidden"><?php bloginfo(); ?></figcaption>

		</figure>

		

		



		<!-- MENU-->

		<nav id="menuOpen">

			<i class="far fa-times-circle" id="closeMenu"></i>

			<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="<?php bloginfo(); ?>">

			<?php wp_nav_menu(); ?>

		</nav>



		<!-- BTN WHTAS -->

		<div class="btnWhts backgroundVerde">

			<a href="https://api.whatsapp.com/send?l=pt&phone=55<?php echo $configuracao['contato_whats'] ?>" target="_blank">Olá!</a>

		</div>



	</div>

</header>