<?php
/**
 * Template Name: Planos
 * Description: Página de Planos
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package impulsefix
 */
get_header();
?>

<!-- PÁGINA DE PLANOS -->
<div class="pg pg-contrate1 planos">
	<section class="secao-planos">
		<h6 class="hidden">SEÇÃO PLANOS</h6>
		<div class="containerLarge">
			<h1 class="tituloPagina">Contrate a ImpuseFix</h1>
			<span class="textoPagina">Encontre o serviço que você precisa!</span>
			<ul>
				<li>
					<img src="<?php echo get_template_directory_uri(); ?>/img/icone-plano1.svg" alt="" class="icone">
					<h5>Plano ImpulseFix Basic:</h5>
					<ul class="ul-info-plano">
						<li class="li-info-plano">Mensalidade pós-paga</li>
						<li class="li-info-plano">Adesão única</li>
						<li class="li-info-plano">Acesso a todos os seguimentos</li>
						<li class="li-info-plano">Menor preço por ponto exectutável</li>
						<li class="li-info-plano">Custo deslocamento mínimo</li>
						<li class="li-info-plano">Agendamentos</li>
					</ul>
					<div class="btnLink backgroundVerde">
						<a href="#">Quero esse plano</a>
					</div>
				</li>
				<li>
					<span class="mais-vendido"><img src="<?php echo get_template_directory_uri(); ?>/img/star.svg" alt="">Mais vendido</span>
					<img src="<?php echo get_template_directory_uri(); ?>/img/icone-plano2.svg" alt="" class="icone">
					<h5>Plano ImpulseFix Premium:</h5>
					<ul class="ul-info-plano">
						<li class="li-info-plano">Mensalidade pré-paga</li>
						<li class="li-info-plano">Sem adesão</li>
						<li class="li-info-plano">Acesso a todos os seguimentos</li>
						<li class="li-info-plano">Sem custo ponto exectutável</li>
						<li class="li-info-plano">Deslocamento reduzido</li>
						<li class="li-info-plano">Orçamento diferenciado para reforma</li>
						<li class="li-info-plano">Agendamentos prioritários</li>
					</ul>
					<div class="btnLink backgroundVerde">
						<a href="#">Quero esse plano</a>
					</div>
				</li>
			</ul>
		</div>
	</section>
	<div class="containerLarge">
		<hr>
		<section class="prestadorServico">
			<h1 class="subTitulo">Ligamos para você!</h1>
			<p class="textoPagina">Preencha o formulário</p>
			<div class="formulario">
				<input type="text" placeholder="Nome Completo">
				<input type="tel" name="" placeholder="Telefone">
				<input type="email" placeholder="E-mail">
				<select>
					<option>Qual serviço você prefere?</option>
					<option>1</option>
					<option>2</option>
					<option>3</option>
				</select>
				<div class="row">
					<div class="col-xs-6">
					  <input type="text" placeholder="CEP">
					</div>			
					<div class="col-xs-6">
						
						<input type="text" placeholder="Estado">
					</div>
				</div>
				<input type="submit" value="Pronto" name="">
			</div>
		</section>
	</div>
</div>

<?php get_footer();