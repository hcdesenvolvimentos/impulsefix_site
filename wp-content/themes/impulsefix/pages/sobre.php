<?php

/**

 * Template Name: Sobre

 * Description: Página Sobre

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package impulsefix

 */

get_header();

?>



<!-- PÁGINA QUEM SOMOS  -->

<div class="pg pg-quemSomos">

	<section class="bannerDestaqueInternas skewTrue">

		<h4 class="hidden"><?php echo get_the_title(); ?></h4>

		<div class="containerLarge skewFalse">

			<article>

				<h6 class="tituloPagina"><?php echo get_the_title(); ?></h6>



				<?php 	while ( have_posts() ) :

				the_post(); ?>

					<div class="textoPagina"><?php echo get_the_content(); ?></div>

				<?php endwhile; ?>

			</article>

			<figure>

				<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title(); ?>">

				 <figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>

			</figure>

		</div>

	</section>

	<section class="quemSomos">

		<h4 class="hidden"><?php echo $configuracao['quemSomos_titulo'] ?></h4>

		<div class="row containerLarge">

			<div class="col-sm-6">

				<article>

					<h6><?php echo $configuracao['quemSomos_titulo'] ?></h6>

					<p class="textoSubtitulo"><?php echo $configuracao['quemSomos_descricao'] ?></p>

				</article>

			</div>	

			<div class="col-sm-6">

				<div class="valores">

					<ul>

						<li>

							<img src="<?php echo get_template_directory_uri(); ?>/img/viao_titulo.svg" alt="<?php echo $configuracao['viao_titulo'] ?>">

							<h3><?php echo $configuracao['viao_titulo'] ?></h3>

							<p><?php echo $configuracao['viao_descricao'] ?></p>

						</li>

						<li>

							<img src="<?php echo get_template_directory_uri(); ?>/img/valores_titulo.svg" alt="<?php echo $configuracao['viao_titulo'] ?>">

							<h3><?php echo $configuracao['valores_titulo'] ?></h3>

							<p><?php echo $configuracao['valores_descricao'] ?></p>

						</li>

						<li>

							<img src="<?php echo get_template_directory_uri(); ?>/img/missao_titulo.png" alt="<?php echo $configuracao['viao_titulo'] ?>">

							<h3><?php echo $configuracao['missao_titulo'] ?></h3>

							<p><?php echo $configuracao['missao_descricao'] ?></p>

						</li>

					</ul>

				</div>

			</div>

		</div>

	</section>

	<section class="comoFUnciona">

		<h4 class="hidden"><?php echo $configuracao['comoFunciona__titulo'] ?></h4>

		<div class="containerLarge">

			<figure class="hidden">

				<img src="<?php echo $configuracao['quemSomos_foto_ilustrativa']['url']?>" alt="<?php echo $configuracao['contato_whats'] ?>">

				 <figcaption class="hidden"><?php echo $configuracao['comoFunciona__titulo'] ?></figcaption>

			</figure>

			<div class="row">

				<div class="col-sm-6">

					<div class="areaTexto">

						<h6 class="subTitulo"><?php echo $configuracao['comoFunciona__titulo'] ?></h6>
						<div class="areaVideo tituloMobile">
							<img src="<?php echo $configuracao['comoFunciona_img']['url'] ?>" alt="<?php echo $configuracao['comoFunciona_img']['url'] ?>">
							<button id="playVideo"><img src="<?php echo get_template_directory_uri(); ?>/img/play.png" alt="Play"></button>

							<video  controls id="video" style="background:url(<?php echo $configuracao['comoFunciona__img']['url'] ?>)">

								<source src="<?php echo $configuracao['comoFunciona__video'] ?>" type="video/mp4">

							</video>

						</div>
						<p class="textoSubtitulo"><?php echo $configuracao['comoFunciona_descricao'] ?></p>

						<div class="btnLink ">

							<a href="<?php echo $configuracao['comoFunciona__link'] ?>" class="backgroundLaranja">Encontre seu plano agora!</a>

						</div>

					</div>

				</div>

				<div class="col-sm-6">

					<div class="areaVideo tituloDesktop">
						<img src="<?php echo $configuracao['comoFunciona_img']['url'] ?>" alt="<?php echo $configuracao['comoFunciona_img']['url'] ?>">
						<button id="playVideo"><img src="<?php echo get_template_directory_uri(); ?>/img/play.png" alt="Play"></button>

						<video  controls id="video" style="background:url(<?php echo $configuracao['comoFunciona__img']['url'] ?>)">

						<source src="<?php echo $configuracao['comoFunciona__video'] ?>" type="video/mp4">

					</video>

				</div>

			</div>

		</div>

	</section>

</div>



<?php get_footer();