<?php

/**

 * Template Name: Inicial

 * Description: Página Inicial

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package impulsefix

 */

get_header();

?>




<!-- PÁGINA INICIAL -->
<div class="pg pg-inicial">

	

	<!-- SESSÃO DESTQUE -->

	<section class="sessaoDestaque">

		<h4 class="hidden"><?php echo $configuracao['destaque_titulo'] ?></h4>



		<div class="containerLarge">

			<div class="carrosselDestaque">

				<div class="item">

					<div class="row">

						<div class="col-sm-6">

							<div class="areaTextoCarrosselDestaque">

								<h2 class="tituloPagina"><?php echo $configuracao['destaque_titulo'] ?></h2>

								<p class="textoPagina"><?php echo $configuracao['destaque_texto'] ?></p>

							</div>

						</div>

						<div class="col-sm-6">

							<figure>

								<img src="<?php echo $configuracao['destaque_foto']['url'] ?>" alt="<?php echo $configuracao['destaque_titulo'] ?>">

								<figcaption class="hidden"><?php echo $configuracao['destaque_titulo'] ?></figcaption>

							</figure>

						</div>

					</div>

				</div>

			</div>

		</div>



		<div class="containerLarge">

			<div class="fomrularioCarrosselDestaque">

				<!-- <div class="areainput">

					<select name="" id="" class="styleInputs">

						<option value="">Selecione o serviço</option>

						<option value="">Elétrica</option>

						<option value="">Hidráulica</option>

						<option value="">Instalação</option>

						<option value="">Reparos</option>

					</select>

					<input  placeholder="Nome" type="text" name="name" class="styleInputs">

					<input type="text" name="email" placeholder="E-mail" class="styleInputs">

					<input type="tel" placeholder="Telefone" class="styleInputs">

				</div>

				<input type="submit" class="btnSubmit" value="Faça seu agendamento!"> -->

				<?php echo do_shortcode('[contact-form-7 id="57" title="Faça seu agendamento"]'); ?>
			</div>

		</div>



	</section>



	<section class="sessaoComofunciona skew">

		<div class="row containerLarge skewCorrecao">
			<h4 class="subTitulo tituloMobile"><?php echo $configuracao['comoFunciona_titulo'] ?></h4>
			<div class="col-sm-7">

				<div class="areaVideo">
					<img src="<?php echo $configuracao['comoFunciona_img']['url'] ?>" alt="<?php echo $configuracao['comoFunciona_img']['url'] ?>">
					<button id="playVideo"><img src="<?php echo get_template_directory_uri(); ?>/img/play.png" alt="Play"></button>

					<video controls  id="video" style="background:url(<?php echo $configuracao['comoFunciona_img']['url'] ?>)">

					<source src="<?php echo $configuracao['comoFunciona_video'] ?>" type="video/mp4">

					<source src="<?php echo $configuracao['comoFunciona_video'] ?>" type="video/ogg">

				</video>

			</div>

			</div>

			<div class="col-sm-5">

				<article>

					<h2 class="subTitulo tituloDesktop"><?php echo $configuracao['comoFunciona_titulo'] ?></h2>

					<p class="textoSubtitulo"><?php echo $configuracao['comoFunciona_texto'] ?></p>

					<div class="btnLink">

						<a href="<?php echo $configuracao['comoFunciona_link'] ?>">Contrate a impulsefix!</a>

					</div>

				</article>

			</div>

		</div>

	</section>



	<section class="sessaoServicos paddingSessao">

		<div class="skewCorrecao">

			<h4 class="titulosSessa text-center"><?php echo $configuracao['servico_titulo'] ?></h4>

			<p class="text-center"><?php echo $configuracao['servico_descricao'] ?></p>

			<div class="containerFull">

				<ul>

					<?php 

						//LOOP DE POST SERVIÇOS

						$servicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );

						while ( $servicos->have_posts() ) : $servicos->the_post();

					 ?>

					<li class="hvr-float-shadow">
						<a href="<?php echo get_permalink() ?>">
							<figure>

								<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">

								 <figcaption class="hidden"><?php echo get_the_title() ?></figcaption>

							</figure>

							<h2><?php echo get_the_title() ?></h2>
							<div class="itens">
								<?php  
									$servico_itens = rwmb_meta('ImpilseFix_servico_itens'); 
									foreach ($servico_itens as $servico_itens):
										$servico_itens = $servico_itens;
								?>
								<p><?php echo $servico_itens ?></p>
								<?php endforeach; ?>
							</div>
						</a>
					</li>


					<?php endwhile; wp_reset_query(); ?>

				</ul>

			</div>
			

			<div class="btnLinkModelo2 text-center">

				<a href="<?php echo $configuracao['servico_link'] ?>" class="btnSubmit">Confira todos os nossos serviços!</a>

			</div>
		</div>



	</section>



	<section class="sessaoContrate">

		<h4 class="hidden"><?php echo $configuracao['condominio_titulo'] ?></h4>



		<div class="row containerLarge skewCorrecao">

			<div class="col-sm-6  col-md-4">

				<article class="areaTexto">

					

					<h6 class="titulosSessa"><?php echo $configuracao['condominio_titulo'] ?></h6>

					

					<p><?php echo $configuracao['condominio_texto'] ?></p>



					<div class="btnLink">

						<a href="<?php echo $configuracao['condominio_link'] ?>">Contrate a impulsefix!</a>

					</div>



				</article>	

			</div>

			<div class="col-sm-6  col-md-8">

				<figure>

					<img src="<?php echo $configuracao['condominio_foto']['url'] ?>" alt="<?php echo $configuracao['condominio_titulo'] ?>">

					 <figcaption class="hidden"><?php echo $configuracao['condominio_titulo'] ?></figcaption>

				</figure>

			</div>

		</div>

		

	</section>



	<section class="sessaoPrestadorServico">

		<h4 class="hidden"><?php echo $configuracao['prestadorServico_titulo'] ?></h4>

		<div class="containerFull" style="max-width: 1800px;">

			<div class="row">

				<div class="col-sm-6">		

					<figure>

						<img src="<?php echo $configuracao['prestadorServico_foto']['url'] ?>" alt="<?php echo $configuracao['prestadorServico_titulo'] ?>">

						 <figcaption class="hidden"><?php echo $configuracao['prestadorServico_titulo'] ?></figcaption>

					</figure>

				</div>



				<div class="col-sm-6">

					<div class="areaTexto">

					

						<h6 class="titulosSessa"><?php echo $configuracao['prestadorServico_titulo'] ?></h6>

					

						<p><?php echo $configuracao['prestadorServico_texto'] ?></p>

						



						<div class="btnLinkModelo2">

							<a href="<?php echo $configuracao['prestadorServico_link'] ?>">Trabalhe Conosco</a>

						</div>

					</div>

				</div>

			</div>
		</div>



	</section>



	<section class="sessaoDeposimentos">

		<h4 class="hidden">Depoimentos</h4>



		<div class="btnCarrossel containerLarge">

			<button class="esquerdaCarrossel"><img src="<?php echo get_template_directory_uri(); ?>/img/left.png"></button>

			<button class="direitaCarrossel"><img src="<?php echo get_template_directory_uri(); ?>/img/right.png"></button>

		</div>

		<div class="areaCarrossel" id="carrosselDepoimentos">

			<?php 

				//LOOP DE POST SERVIÇOS

				$servicos = new WP_Query( array( 'post_type' => 'depoimento', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );

				while ( $servicos->have_posts() ) : $servicos->the_post();

			 ?>

			<div class="item">

				<figure>

					<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">

					 <figcaption><?php echo get_the_title() ?></figcaption>

				</figure>

				<h2  class="hidden"><?php echo get_the_title() ?></h2>

				<p><?php echo get_the_content(); ?></p>

			</div>

			<?php endwhile; wp_reset_query(); ?>

			

		</div>

	</section>



</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.wpcf7-submit').click(function(){
			function show_popup(){

				if($("div.wpcf7-validation-errors").length >= 1) {
					$("#modalFormularioError").show();
					clearInterval(robo);
					console.log("Error")
				}
				if($("div.wpcf7-mail-sent-ok").length >= 1) {
					$("#modalFormularioSucess").show();
					clearInterval(robo);
					console.log("TOp")
				}
			};

			var robo = setInterval( show_popup, 500 );

		});

		$("#modalFormularioError").click(function(){
			$("#modalFormularioError").hide();
		});

		$("#modalFormularioSucess").click(function(){
			$("#modalFormularioSucess").hide();
		});

	});
</script>


<?php get_footer();