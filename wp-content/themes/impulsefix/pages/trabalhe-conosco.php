<?php
/**
 * Template Name: Trabalhe Conosco
 * Description: Trabalhe Conosco
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package impulsefix
 */
get_header();
?>
<!-- PÁGINA QUEM SOMOS  -->
<div class="pg pg-prestadorServico">
	
	<div class="containerLarge">
		<section class="prestadorServico">	
			<h1 class="tituloPagina"><?php echo $configuracao['trabalhe_conosco_titulo'] ?></h1>
			<p class="textoPagina"><?php echo $configuracao['trabalhe_conosco_texto'] ?></p>
			<hr>

			<span class="subTitulo"><?php echo $configuracao['trabalhe_conosco_titulo_formulario'] ?></span>
			<?php echo do_shortcode('[contact-form-7 id="47" title="Trabalhe Conosco"]'); ?>
		</section>
		<hr>

		<section class="areaBeneficios">
			<div class="row">
				<div class="col-sm-6">
					<div class="areaSelecao">
						<h3 class="subTitulo"><?php echo $configuracao['trabalhe_conosco_titulo_beneficios'] ?></h3>
						<ul>
							<?php 
								$i = 0;
								//LOOP DE POST SERVIÇOS
								$beneficio = new WP_Query( array( 'post_type' => 'beneficio', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
								while ( $beneficio->have_posts() ) : $beneficio->the_post();
									if ($i == 0):
							 ?>
							<li>
								<button data-id="<?php echo $i ?>" class="ativo"><?php echo get_the_title(); ?></button>
							</li>
							<?php else: ?>
							<li>
								<button data-id="<?php echo $i ?>"><?php echo get_the_title(); ?></button>
							</li>
							<?php endif; $i++; endwhile; wp_reset_query(); ?>
						</ul>
					</div>
				</div>
				<div class="col-sm-6">
					<?php 
						$j = 0;
						//LOOP DE POST SERVIÇOS
						$beneficio = new WP_Query( array( 'post_type' => 'beneficio', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
						while ( $beneficio->have_posts() ) : $beneficio->the_post();
							if ($j == 0):
					 ?>

					<article id="<?php echo $j ?>" class="ativo">
						<?php echo the_content(); ?>
					</article>

					<?php else: ?>

					<article id="<?php echo $j ?>">
						<?php echo the_content(); ?>
					</article>
					
					<?php endif;  $j++; endwhile; wp_reset_query(); ?>
				</div>
			</div>
		</section>

	</div>
</div>
<?php get_footer();