<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_impulsefix_site' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'p+Q6H*J|G>4?`a3!|uQillFnk1:D5i+xRnN_(>qgM*.Ado-p?{cuX:ny`J<h^mQz' );
define( 'SECURE_AUTH_KEY',  '[U}]NwF+~o;B)iYU|<1b,4@-{_Wx/hsUvu-M^f[p(1Erxe [6o^+W[?i.)ft:fu%' );
define( 'LOGGED_IN_KEY',    '?QZtkl]UsD_%zYS`ViR+qahB+Gnq=/XGbOmK:Er!5a=Aa(nJ.|5O>1*WD<_5E4T6' );
define( 'NONCE_KEY',        '(~`Fiw)?W!Py;MF%qVG}:aIo @z~(;5R~su{$KM[WE[IWUsJ/G`?w{lkou1?+?Ov' );
define( 'AUTH_SALT',        'W+|O(8niY@?Ku+m-O1b_>#T&KE<f/-7j!Tfr~_9Pbsu>b<[E]%`9CJVJ$N]46o~e' );
define( 'SECURE_AUTH_SALT', 'c05h8_;X!`#rSveLw*lY;{us1xv4<`,,H$skpCl)m76U&E{owL,H.Was,9^P%2,c' );
define( 'LOGGED_IN_SALT',   'zYo$X=?n0)IJ-B%q#<Rwq;-y#UN= PnKj1Y7@0=#}1!G RQ)YDNuHEO+MVtX3u?i' );
define( 'NONCE_SALT',       'NgNT]/kETgtzL qO<vw&&6YV|^@lEL+}<ph2D/E,Q2n,kPe3_4nKvORr_d3!X`@k' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'if_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
